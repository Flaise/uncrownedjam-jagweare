﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreAdvance : MonoBehaviour
{

    public int Score;
    public int TargetScore;
    public GameObject successor;

    void Update() {
        if (Score >= TargetScore) {
            gameObject.SetActive(false);

            if (successor != null) {
                successor.SetActive(true);
            }
        }
    }
}
