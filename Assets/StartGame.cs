﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{

    public GameObject warriorPrefab;
    public GameObject successor;
    public GameObject fighters;
    public AudioSource music;
    public AudioSource[] shush;
    public GameObject successorWin;
    public GameObject successorLose;

    void Start() {
    }

    void Update()
    {
        if (music != null) {
            music.Play();
        }
        foreach (AudioSource source in shush) {
            if (source != null) {
                source.Stop();
            }
        }

        if (fighters != null) {
            while (fighters.transform.childCount > 0) {
                DestroyImmediate(fighters.transform.GetChild(0).gameObject);
            }

            if (warriorPrefab != null) {
                GameObject a = Instantiate(warriorPrefab, new Vector3(0, -2, 0), Quaternion.identity);
                a.transform.parent = fighters.transform;
                // a.GetComponent<ScoreAdvance>();

                ScoreAdvance comp = (ScoreAdvance)a.GetComponent(typeof(ScoreAdvance));
                comp.successor = successorWin;

                // PlayerControl comp = (PlayerControl)player.GetComponent(typeof(PlayerControl));
            }
        }

        gameObject.SetActive(false);
        
        if (successor != null) {
            successor.SetActive(true);
        }
    }
}
