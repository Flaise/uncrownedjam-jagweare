﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMe : MonoBehaviour
{
    public AudioSource ouch;
    public ScoreAdvance score;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            if (ouch != null) {
                ouch.Play();
            }
            if (score != null) {
                score.Score += 1;
            }
            Destroy(gameObject);
        }
    }
}
