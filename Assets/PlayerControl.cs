﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{

    public bool running = false;
    public float speed = 1;
    public GameObject rotateMe;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        if (!running) {
            return;
        }

        if (rotateMe != null) {
            Vector3 mouseScreen = Input.mousePosition;
            Vector3 mouse = Camera.main.ScreenToWorldPoint(mouseScreen);
            rotateMe.transform.rotation = Quaternion.Euler(0, 0,
                Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x)
                * Mathf.Rad2Deg + 90);
        }

        float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		Vector3 up = -transform.up * v * speed * Time.deltaTime;
		Vector3 right = -transform.right * h * speed * Time.deltaTime;

        transform.position += up + right;
        
        Camera.main.orthographicSize = 15;
        Camera.main.transform.position = transform.position;
    }
}
