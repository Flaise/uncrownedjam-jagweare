﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogAdvance : MonoBehaviour
{
    public GameObject successor;
    public AudioSource voice;
    public AudioSource[] shush;
    public Vector3 cameraPosition;
    public bool updateCamera;

    void Start()
    {
        
    }

    void OnEnable()
    {
        if (voice != null) {
            voice.Play();
        }
        foreach (AudioSource source in shush) {
            if (source != null) {
                source.Stop();
            }
        }
        if (updateCamera) {
            Camera.main.transform.position = cameraPosition;
        }
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            gameObject.SetActive(false);

            if (successor != null) {
                successor.SetActive(true);
            }
        }
    }
}
