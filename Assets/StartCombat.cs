﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCombat : MonoBehaviour
{
    public AudioSource music;
    public AudioSource shush;

    void Start()
    {
        
    }

    void Update()
    {
        if (music != null) {
            music.Play();
        }
        if (shush != null) {
            shush.Stop();
        }
        
        var player = GameObject.FindWithTag("Player");
        if (player != null) {
            PlayerControl comp = (PlayerControl)player.GetComponent(typeof(PlayerControl));
            if (comp) {
                comp.running = true;
            }
        }

        gameObject.SetActive(false);
        
    }
}
