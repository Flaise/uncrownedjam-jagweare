﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopyLoop : MonoBehaviour {
    AudioSource Source;
    public float LoopEndTime;
    public float LoopStartTime;

    void Start() {
        Source = GetComponent<AudioSource>();
    }

    void Update() {
        if (Source.timeSamples >= LoopEndTime * Source.clip.frequency) {
            // if the player has gone past the end of the loop
            Source.timeSamples -= Mathf.RoundToInt((LoopEndTime - LoopStartTime) * Source.clip.frequency);
            // go back in time by the number of samples in the loop
        }
    }
}
